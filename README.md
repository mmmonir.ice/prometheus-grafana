# Setup Alerts and monitoring on the CPU/Memory for the cluster using Prometheus/Grafana:
Below are some quick notes on how I setup helm, prometheus and grafana on a Kubernetes cluster using helm.

# kube-prometheus-stack

Installs the [kube-prometheus stack](https://github.com/prometheus-operator/kube-prometheus), a collection of Kubernetes manifests, [Grafana](http://grafana.com/) dashboards, and [Prometheus rules](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/) combined with documentation and scripts to provide easy to operate end-to-end Kubernetes cluster monitoring with [Prometheus](https://prometheus.io/) using the [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator).

See the [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus) README for details about components, dashboards, and alerts.

_Note: This chart was formerly named `prometheus-operator` chart, now renamed to more clearly reflect that it installs the `kube-prometheus` project stack, within which Prometheus Operator is only one component._

## Prerequisites

- Kubernetes 1.16+
- Helm 3+

## Have a K8S cluster already
```bash
root@lb-postgresql:/usr/local/manifests# kubectl get nodes -o wide
NAME       STATUS   ROLES    AGE    VERSION   INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
kmaster1   Ready    master   363d   v1.19.2   78.141.231.140    <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
kmaster2   Ready    master   363d   v1.19.2   45.77.91.124      <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
kworker1   Ready    <none>   363d   v1.19.2   192.248.169.217   <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
kworker2   Ready    <none>   363d   v1.19.2   78.141.201.105    <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
kworker3   Ready    <none>   112d   v1.19.2   192.248.147.199   <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
kworker4   Ready    <none>   68d    v1.19.2   208.85.21.249     <none>        Ubuntu 20.04.2 LTS   5.4.0-71-generic   docker://19.3.10
root@lb-postgresql:/usr/local/manifests#
```

## Install helm on kmaster1:
```bash
# curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
# apt-get install apt-transport-https --yes
# echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
# sudo apt-get update
# sudo apt-get install helm
```
## Get Helm Repository Info,
```bash
root@lb-postgresql:/usr/local/manifests# helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
"prometheus-community" has been added to your repositories

root@lb-postgresql:/usr/local/manifests# helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
...Successfully got an update from the "jetstack" chart repository
...Successfully got an update from the "prometheus-community" chart repository
Update Complete. ⎈Happy Helming!⎈
root@lb-postgresql:/usr/local/manifests#
```

_See [`helm repo`](https://helm.sh/docs/helm/helm_repo/) for command documentation._

## Create Namespace
Create a namespace for keeping the charts in its own namespace,
```bash
root@lb-postgresql:/usr/local/manifests# kubectl create ns monitoring
namespace/monitoring created
root@lb-postgresql:/usr/local/manifests#
```

## Install Helm Chart (prometheus-community/kube-prometheus-stack)

```bash
root@lb-postgresql:/usr/local/manifests# helm install prometheus prometheus-community/kube-prometheus-stack -n monitoring
NAME: prometheus
LAST DEPLOYED: Tue May 17 14:13:56 2022
NAMESPACE: monitoring
STATUS: deployed
REVISION: 1
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "release=prometheus"

Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.
root@lb-postgresql:/usr/local/manifests#
root@lb-postgresql:/usr/local/manifests# helm list -n monitoring
NAME      	NAMESPACE 	REVISION	UPDATED                               	STATUS  	CHART                       	APP VERSION
prometheus	monitoring	1       	2022-05-17 14:13:56.65336589 +0000 UTC	deployed	kube-prometheus-stack-35.2.0	0.56.2
root@lb-postgresql:/usr/local/manifests#
```

See [configuration](#configuration) below._

See [helm install](https://helm.sh/docs/helm/helm_install/) for command documentation.

## Check all the objects created:
```bash
root@lb-postgresql:/usr/local/manifests# kubectl get all -n monitoring
NAME                                                         READY   STATUS    RESTARTS   AGE
pod/alertmanager-prometheus-kube-prometheus-alertmanager-0   2/2     Running   0          2m53s
pod/prometheus-grafana-8565b5787c-g7ncf                      3/3     Running   0          2m56s
pod/prometheus-kube-prometheus-operator-84c7d895d-fcxhv      1/1     Running   0          2m56s
pod/prometheus-kube-state-metrics-847b7f5749-kkt2h           1/1     Running   0          2m56s
pod/prometheus-prometheus-kube-prometheus-prometheus-0       2/2     Running   0          2m53s
pod/prometheus-prometheus-node-exporter-7bpnp                1/1     Running   0          2m56s
pod/prometheus-prometheus-node-exporter-fvlfm                1/1     Running   0          2m56s
pod/prometheus-prometheus-node-exporter-mjnf7                1/1     Running   0          2m56s
pod/prometheus-prometheus-node-exporter-s2sm4                1/1     Running   0          2m56s
pod/prometheus-prometheus-node-exporter-txx2k                1/1     Running   0          2m56s
pod/prometheus-prometheus-node-exporter-x4xsp                1/1     Running   0          2m56s

NAME                                              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
service/alertmanager-operated                     ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   2m54s
service/prometheus-grafana                        ClusterIP   10.105.32.42     <none>        80/TCP                       2m57s
service/prometheus-kube-prometheus-alertmanager   ClusterIP   10.98.8.24       <none>        9093/TCP                     2m56s
service/prometheus-kube-prometheus-operator       ClusterIP   10.98.32.62      <none>        443/TCP                      2m56s
service/prometheus-kube-prometheus-prometheus     ClusterIP   10.104.205.150   <none>        9090/TCP                     2m56s
service/prometheus-kube-state-metrics             ClusterIP   10.101.122.0     <none>        8080/TCP                     2m56s
service/prometheus-operated                       ClusterIP   None             <none>        9090/TCP                     2m53s
service/prometheus-prometheus-node-exporter       ClusterIP   10.110.226.45    <none>        9100/TCP                     2m56s

NAME                                                 DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/prometheus-prometheus-node-exporter   6         6         6       6            6           <none>          2m56s

NAME                                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/prometheus-grafana                    1/1     1            1           2m56s
deployment.apps/prometheus-kube-prometheus-operator   1/1     1            1           2m56s
deployment.apps/prometheus-kube-state-metrics         1/1     1            1           2m56s

NAME                                                            DESIRED   CURRENT   READY   AGE
replicaset.apps/prometheus-grafana-8565b5787c                   1         1         1       2m56s
replicaset.apps/prometheus-kube-prometheus-operator-84c7d895d   1         1         1       2m56s
replicaset.apps/prometheus-kube-state-metrics-847b7f5749        1         1         1       2m56s

NAME                                                                    READY   AGE
statefulset.apps/alertmanager-prometheus-kube-prometheus-alertmanager   1/1     2m53s
statefulset.apps/prometheus-prometheus-kube-prometheus-prometheus       1/1     2m53s
root@lb-postgresql:/usr/local/manifests#
```
## Get the dashboard port information and default user created
```bash
root@lb-postgresql:/usr/local/manifests# kubectl get pods -o=custom-columns=NameSpace:.metadata.namespace,NAME:.metadata.name,CONTAINERS:.spec.containers[*].name -n monitoring
NameSpace    NAME                                                     CONTAINERS
monitoring   alertmanager-prometheus-kube-prometheus-alertmanager-0   alertmanager,config-reloader
monitoring   prometheus-grafana-8565b5787c-g7ncf                      grafana-sc-dashboard,grafana-sc-datasources,grafana
monitoring   prometheus-kube-prometheus-operator-84c7d895d-fcxhv      kube-prometheus-stack
monitoring   prometheus-kube-state-metrics-847b7f5749-kkt2h           kube-state-metrics
monitoring   prometheus-prometheus-kube-prometheus-prometheus-0       prometheus,config-reloader
monitoring   prometheus-prometheus-node-exporter-7bpnp                node-exporter
monitoring   prometheus-prometheus-node-exporter-fvlfm                node-exporter
monitoring   prometheus-prometheus-node-exporter-mjnf7                node-exporter
monitoring   prometheus-prometheus-node-exporter-s2sm4                node-exporter
monitoring   prometheus-prometheus-node-exporter-txx2k                node-exporter
monitoring   prometheus-prometheus-node-exporter-x4xsp                node-exporter
root@lb-postgresql:/usr/local/manifests#
```
Note from above the POD of interest "prometheus-grafana-8565b5787c-g7ncf" and the container of interest is "grafana".
Get the HTTP port number and user info:
```bash 
root@lb-postgresql:/usr/local/manifests# kubectl logs prometheus-grafana-8565b5787c-g7ncf -c grafana -n monitoring | grep -E "Listen|default admin"
logger=sqlstore t=2022-05-17T14:14:09.63+0000 lvl=info msg="Created default admin" user=admin
logger=http.server t=2022-05-17T14:14:09.73+0000 lvl=info msg="HTTP Server Listen" address=[::]:3000 protocol=http subUrl= socket=
```

Password for grafana is "prom-operator" lookup from here:
`
https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml
`
Review Grafana dashboard by just using the POD and Create a quick SVC to just use the Grafana deployment on a Loadbalancer. 

```bash
root@lb-postgresql:/usr/local/manifests# kubectl get pod -n monitoring -l app.kubernetes.io/name=grafana
NAME                                  READY   STATUS    RESTARTS   AGE
prometheus-grafana-8565b5787c-g7ncf   3/3     Running   0          9m12s
root@lb-postgresql:/usr/local/manifests#


root@lb-postgresql:/usr/local/manifests# kubectl get deployment -n monitoring -l app.kubernetes.io/name=grafana
NAME                 READY   UP-TO-DATE   AVAILABLE   AGE
prometheus-grafana   1/1     1            1           9m54s
root@lb-postgresql:/usr/local/manifests#


root@lb-postgresql:/usr/local/manifests# kubectl expose deployment prometheus-grafana -n monitoring --name=prometheus-svc --port=3000 --type=NodePort
service/prometheus-svc exposed
root@lb-postgresql:/usr/local/manifests#

root@lb-postgresql:/usr/local/manifests# kubectl get svc -n monitoring | grep -i prometheus-svc
prometheus-svc                            NodePort    10.96.84.127     <none>        3000:30171/TCP               2m36s
root@lb-postgresql:/usr/local/manifests#


root@lb-postgresql:/usr/local/manifests# kubectl patch svc prometheus-svc -n monitoring -p '{"spec": {"type": "LoadBalancer", "externalIPs":["78.141.231.140"]}}'
service/prometheus-svc patched
root@lb-postgresql:/usr/local/manifests#
root@lb-postgresql:/usr/local/manifests# kubectl get svc -n monitoring
NAME                                      TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)                      AGE
alertmanager-operated                     ClusterIP      None             <none>           9093/TCP,9094/TCP,9094/UDP   19m
prometheus-grafana                        ClusterIP      10.105.32.42     <none>           80/TCP                       19m
prometheus-kube-prometheus-alertmanager   ClusterIP      10.98.8.24       <none>           9093/TCP                     19m
prometheus-kube-prometheus-operator       ClusterIP      10.98.32.62      <none>           443/TCP                      19m
prometheus-kube-prometheus-prometheus     ClusterIP      10.104.205.150   <none>           9090/TCP                     19m
prometheus-kube-state-metrics             ClusterIP      10.101.122.0     <none>           8080/TCP                     19m
prometheus-operated                       ClusterIP      None             <none>           9090/TCP                     19m
prometheus-prometheus-node-exporter       ClusterIP      10.110.226.45    <none>           9100/TCP                     19m
prometheus-svc                            LoadBalancer   10.96.84.127     78.141.231.140   3000:30171/TCP               8m54s
```
## Login 
Now in a browser go to any of the loadbalancer IP and port 3000 to get into the grafana dashboard
In my cluster I went to :
http://78.141.231.140:3000/login

## Helmchart Upgrade
```bash
root@lb-postgresql:/usr/local# helm upgrade prometheus prometheus-community/kube-prometheus-stack -n monitoring
Release "prometheus" has been upgraded. Happy Helming!
NAME: prometheus
LAST DEPLOYED: Tue May 17 15:55:36 2022
NAMESPACE: monitoring
STATUS: deployed
REVISION: 2
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "release=prometheus"

Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.
root@lb-postgresql:/usr/local# kubectl get po -n monitoring
```

## Password Change:
Change the secrets and apply
```bash
root@lb-postgresql:/usr/local# kubectl get secrets prometheus-grafana -n monitoring -o yaml > grafana-admin.yaml
root@lb-postgresql:/usr/local# cat grafana-admin.yaml
apiVersion: v1
data:
  admin-password: cHJvbS1vcGVyYXRvcg==
  admin-user: YWRtaW4=
  ldap-toml: ""
kind: Secret
metadata:
  annotations:
    meta.helm.sh/release-name: prometheus
    meta.helm.sh/release-namespace: monitoring
  creationTimestamp: "2022-05-17T14:14:05Z"
  labels:
    app.kubernetes.io/instance: prometheus
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: grafana
    app.kubernetes.io/version: 8.5.0
    helm.sh/chart: grafana-6.29.2
  name: prometheus-grafana
  namespace: monitoring
  resourceVersion: "106563833"
  selfLink: /api/v1/namespaces/monitoring/secrets/prometheus-grafana
  uid: 1b05132c-8f46-41e5-90da-8d55b9eefd44
type: Opaque

root@lb-postgresql:/usr/local# echo "admin" | base64
YWRtaW4K

root@lb-postgresql:/usr/local# echo "cHJvbS1vcGV===" | base64 --decode
prodsf
root@lb-postgresql:/usr/local# kubectl apply -f grafana-admin.yaml
```
## Monitoring Dashboard
![Screenshot 2022-05-17 at 8.51.25 PM.png](./assets/Screenshot 2022-05-17 at 8.51.25 PM.png)
![Node-Exporter-USE-Method-Cluster-Grafana.png](./assets/Node-Exporter-USE-Method-Cluster-Grafana.png)
![Node-Exporter-USE-Method-Node-Grafana.png](./assets/Node-Exporter-USE-Method-Node-Grafana.png)

## Dependencies

By default this chart installs additional, dependent charts:

- [prometheus-community/kube-state-metrics](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-state-metrics)
- [prometheus-community/prometheus-node-exporter](https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-node-exporter)
- [grafana/grafana](https://github.com/grafana/helm-charts/tree/main/charts/grafana)

To disable dependencies during installation, see [multiple releases](#multiple-releases) below.

_See [helm dependency](https://helm.sh/docs/helm/helm_dependency/) for command documentation._

## Uninstall Helm Chart

```console
helm uninstall [RELEASE_NAME]
```

## Alerting
